console.log("hi")
const app = document.querySelector("#app");

const url = "https://jsonplaceholder.typicode.com/todos";

const delay = ms => {
    return new Promise(r =>  setTimeout(() =>  r(),ms) )
}

function fatchTodos() {
    console.log('fetch started')
    return delay(2000)
        .then(() => {
            return fetch(url)
        })
        .then(response =>
            response.json())
}

fatchTodos()
    .then(data => {
        console.log('data: ', data )
        app.innerHTML =  data
    })
    .catch(e => console.error(e))

// .then(() => conso÷le.log("4 sec"));
