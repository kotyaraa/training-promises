// const delay = ms => {
//     return new Promise(r =>  setTimeout(() =>  r(),ms) )
// }
// delay(2000).then(() => console.log('2 sec'))
// .then(() => console.log("4 sec"));
//-----------------------------------------------------------------------------------
//
// function delay(ms) {
//   return new Promise((r) => {
//     setTimeout(() => {
//       r();
//     }, ms);
//   });
// }

// async function delays() {
//   try {
//     await delay(2000);
//     console.log("2 seconds");
//     await delay(2000);
//     console.log("4 seconds");
//   } catch (err) {
//     console.log(new Error(err));
//   }
// }
// delays();
//===================================================================================
// const WRING_OUT_TIME = 500;
// const SQUATINGS_TIME = 200;
// function wringOut(count) {
//   return new Promise((resolve, reject) => {
//     if (count > 100) {
//       reject(new Error("pulling up too much"));
//     }
//     setTimeout(() => {
//       resolve();
//     }, count * WRING_OUT_TIME);
//   });
// }

// function squatings(count) {
//   return new Promise((resolve, reject) => {
//     if (count > 1000) {
//       reject(new Error("squatings too much"));
//     }
//     setTimeout(() => {
//       resolve();
//     }, count * SQUATINGS_TIME);
//   });
// }

// console.log("start");

// wringOut(10)
//   .then(() => {
//     console.log("finished 10 wringOut");
//     return squatings(20);
//   })
//   .then(() => {
//     console.log("finished 20 squatings");
//   })
//   .catch((err) => console.log(err.toString()));
//===================================================================================

// const WRING_OUT_TIME = 500;
// const SQUATINGS_TIME = 200;
// function wringOut(count) {
//   return new Promise((resolve, reject) => {
//     if (count > 100) {
//       reject(new Error("pulling up too much"));
//     }
//     setTimeout(() => {
//       resolve();
//     }, count * WRING_OUT_TIME);
//   });
// }

// function squatings(count) {
//   return new Promise((resolve, reject) => {
//     if (count > 1000) {
//       reject(new Error("squatings too much"));
//     }
//     setTimeout(() => {
//       resolve();
//     }, count * SQUATINGS_TIME);
//   });
// }



// async function training() {
    
//     try {
//         console.log("start");
//         await wringOut(10);
//         console.log("finished 10 wringOut");
//         await squatings(20);
//         console.log("finished 20 squatings");
//         return true;
//     }  catch (err) {
//         console.log(err.toString())
//         return false;
//     }
// }
// training((result) => console.log(result));


